var check_program = 4;

switch (check_program) {
  case 1:
    const first_program = require("./daysCountmethod1");

    break;

  case 2:
    const second_program = require("./daysCountmethod2");

    break;

  case 3:
    const third_program = require("./getday");

  case 4:
    const fourth_program = require("./matrix2dmul");

    break;

  case 5:
    const fifth_program = require("./matrix3dmul");

    break;

  case 6:
    const sixth_program = require("./grid");
    break;
}
