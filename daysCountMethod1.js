/*
A new planet was discovered. This is called Planet Alpha. 
Scientists have made a new calendar for this planet and it has only 9 months each and the number of days in each month is below.

January - 30
February - 20
March - 30
April - 25
May - 30
June - 25
July - 30
August - 30
September - 25

Date 02/03/04 means, in the year 04, the 2nd of March. Please note that 26/06/22 is an invalid date, because June has only 25 days. 

Write a function to calculate the difference between two date strings for this planet: "19/02/05" and "22/06/86"
*/
/**
 * This function checks whether the input string is in date format or noot
 * @param {string} dateString  inputs date as string
 * @returns {boolean} True or false
 */
function validate_date(dateString) {
  let dateformat = /^(0?[1-9]|[1-2][0-9]|30)[\/](0?[1-9])[\/]([0-9]*)$/; //date format regex
  if (dateformat.test(dateString)) {
    let datepart = dateString.split("/");
    let day = parseInt(datepart[0]);
    let month = parseInt(datepart[1]);
    let year = parseInt(datepart[2]);
    var monthdays = [30, 20, 30, 25, 30, 25, 30, 30, 25];
    if (day <= monthdays[month - 1]) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

/**
 * this function prints the difference in days between two given dates
 * @param {string} str1 date string1
 * @param {string} str2 date string2
 */
function difference(str1, str2) {
  if (validate_date(str1) && validate_date(str2)) {
    var farr = str1.split("/");
    var tarr = str2.split("/");
    var month = [30, 20, 30, 25, 30, 25, 30, 30, 25];

    //splitting and parsing date string
    let d1 = parseInt(farr[0]);
    let m1 = parseInt(farr[1]);
    let y1 = parseInt(farr[2]);

    let d2 = parseInt(tarr[0]);
    let m2 = parseInt(tarr[1]);
    let y2 = parseInt(tarr[2]);
    //counting days from 00/00/00
    let days1 = y1 * 245 + d1;
    for (let i = 0; i < m1 - 1; i++) {
      days1 = days1 + month[i];
    }

    let days2 = y2 * 245 + d2;
    for (let i = 0; i < m2 - 1; i++) {
      days2 = days2 + month[i];
    }
    //finding difference
    var dif = days1 - days2;

    if (dif >= 0) {
      console.log(dif);
    } else {
      dif = dif * -1;
      console.log(dif);
    }
  } else {
    console.log("invalid");
  }
}

difference("19/02/05", "22/06/86");
