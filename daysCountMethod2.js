/*
A new planet was discovered. This is called Planet Alpha. 
Scientists have made a new calendar for this planet and it has only 9 months each and the number of days in each month is below.

January - 30
February - 20
March - 30
April - 25
May - 30
June - 25
July - 30
August - 30
September - 25

Date 02/03/04 means, in the year 04, the 2nd of March. Please note that 26/06/22 is an invalid date, because June has only 25 days. 

Write a function to calculate the difference between two date strings for this planet: "19/02/05" and "22/06/86"
*/

/**
 * Thisis a  function that checks whether the input string is in date format or noot
 * @param {string} dateString  inputs date as string
 * @returns {boolean} True or false
 */
function validate_date(dateString) {
  let dateformat = /^(0?[1-9]|[1-2][0-9]|30)[\/](0?[1-9])[\/]([0-9]*)$/;
  //date format
  if (dateformat.test(dateString)) {
    //validating input date string against date format
    let datepart = dateString.split("/");
    let day = parseInt(datepart[0]);
    let month = parseInt(datepart[1]);
    let year = parseInt(datepart[2]);
    var monthdays = [30, 20, 30, 25, 30, 25, 30, 30, 25];
    if (day <= monthdays[month - 1]) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}
/**
 * This is a function to find the larger of two date.It checks whether 
 * first string is greater than second string or not
 * @param {string} str1 date string1
 * @param {string} str2 date string2
 * @returns {boolean} true if date string1 is greater
 */
function greater(str1, str2) {
  if (validate_date(str1) && validate_date(str2)) {
    let [day1, month1, year1] = _breakDate(str1);
    let [day2, month2, year2] = _breakDate(str2);

    if (year1 > year2) {
      return 1;
    } else if (year1 < year2) {
      return -1;
    } else {
      if (month1 > month2) {
        return 1;
      } else if (month1 < month2) {
        return -1;
      } else {
        if (day1 > day2) {
          return 1;
        } else if (day2 > day1) {
          return -1;
        } else {
          return 0;
        }
      }
    }
  } else {
    return "invalid";
  }
}
/**
 * this is a function to get the next date of the given date string
 * @param {string} stringDate date as string
 * @returns date , month , year as an array
 */
function getnext(stringDate) {
  if (validate_date(stringDate)) {
    let [date, month, year] = _breakDate(stringDate);

    if (date >= 1 && date < 20) {
      date = date + 1;
      return [date, month, year];
    } else if (date == 20) {
      if (month == 2) {
        date = 1;
        month = month + 1;
        return [date, month, year];
      } else {
        date = date + 1;
        return [date, month, year];
      }
    } else if (date > 20 && date < 25) {
      date = date + 1;
      return [date, month, year];
    } else if (date == 25) {
      if (month == 4 || month == 6 || month == 9) {
        date = 1;
        if (month == 9) {
          month = 1;
          year = year + 1;
          return [date, month, year];
        } else {
          month = month + 1;
          return [date, month, year];
        }
      } else {
        date = date + 1;
        return [date, month, year];
      }
    } else if (date > 25 && date < 30) {
      date = date + 1;
      return [date, month, year];
    } else if (date == 30) {
      date = 1;
      month = month + 1;
      return [date, month, year];
    }
  } else {
    return "invalid";
  }
}

/**
 * This is a helper function to break down the date and return the
 * constituent parts
 * @param {String} input_date
 * @returns {Array} contains day, month and year at indices 0, 1, 2
 */
function _breakDate(input_date) {
  let input_date_split = input_date.split("/");
  let day = parseInt(input_date_split[0]);
  let month = parseInt(input_date_split[1]);
  let year = parseInt(input_date_split[2]);

  return [day, month, year];
}

function difference(str1, str2) {
  if (validate_date(str1) && validate_date(str2)) {
    let input1, input2;
    var count = 0;

    let inputComparisionResults = greater(str1, str2);

    if (inputComparisionResults == 0) {
      return count;
    } else if (inputComparisionResults > 0) {
      input1 = str1;
      input2 = str2;
    } else if (inputComparisionResults < 0) {
      input1 = str2;
      input2 = str1;
    }

    let [day1, month1, year1] = _breakDate(input1);
    let [day2, month2, year2] = _breakDate(input2);

    var nextdate = getnext(input2);
    var nextDateString =
      "" + nextdate[0] + "/" + nextdate[1] + "/" + nextdate[2];
    count = 1;
    while (greater(input1, nextDateString) != 0) {
      count = count + 1;

      // updating the next date
      nextdate = getnext(nextDateString);
      nextDateString = "" + nextdate[0] + "/" + nextdate[1] + "/" + nextdate[2];

      console.log(input1 + "...." + nextDateString);
    }
    return count;
  }
}



var x = difference("19/02/05", "22/06/86");
console.log(x);
