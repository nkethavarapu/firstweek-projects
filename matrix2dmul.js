/**
 * this a function to validate a matrix
 * @param {object} matrix inputs matrix in object format
 * @returns {boolean} true if matrix is vallidated
 */
function validate_matrix(matrix) {
  var [matrix_rows, matrix_cols] = matrix_size(matrix);
  for (var i = 0; i < matrix_rows; i++) {
    if (matrix[i].length != matrix[i + 1].length) {
      return false;
    } else {
      for (var j = 0; j < matrix_rows; j++) {
        for (var k = 0; k < matrix_cols; k++) {
          if (typeof matrix[j][k] !== "number") {
            return false;
          } else {
            continue;
          }
        }
      }
      return true;
    }
  }
}
/**
 * This is a helper function to get number of rows 
 * and colums of matrix
 * @param {object} inputs a 2d matrix
 * @returns {Array} contains matrix rows and columns **/
function matrix_size(matrix) {
  var matrix_rows = matrix.length;
  var matrix_cols = matrix[0].length;
  return [matrix_rows, matrix_cols];
}
/**
 * this is a function to validate whether 2 matrices are fit 
 * for multiplication
 * @param {object} matrix1 matrix1
 * @param {object} matrix2 matrix2
 * @returns  {boolean}
 */
function validate_mul_matrixes(matrix1, matrix2) {
  if (validate_matrix(matrix1) && validate_matrix(matrix2)) {
    var matrix1_cols = matrix1[0].length;
    var matrix2_rows = matrix2.length;
    if (matrix1_cols != matrix2_rows) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
}
function multiplication2d(matrix1, matrix2) {
  if (validate_mul_matrixes(matrix1, matrix2)) {
    var result = [];
    var [matrix1_rows, matrix1_cols] = matrix_size(matrix1);
    var [matrix2_rows, matrix2_cols] = matrix_size(matrix2);

    for (var row = 0; row < matrix1_rows; row++) {
      result[row] = [];
      for (var col = 0; col < matrix2_cols; col++) {
        sum = 0;
        for (var k = 0; k < matrix1_cols; k++) {
          sum += matrix1[row][k] * matrix2[k][col];
        }
        result[row][col] = sum;
      }
    }
    return result;
  } else {
    return "not valid";
  }
}
let matrix1 = [
  [1, 2],
  [3, 8],
];
let matrix2 = [
  [5, 6],
  [7, 8],
];
var result = multiplication2d(matrix1, matrix2);
console.log(result);

