/**
 * This is a helper function to break down the date and return the
 * constituent parts
 * @param {String} input_date
 * @returns {Array} contains day, month and year at indices 0, 1, 2
 */
 function _breakDate(input_date) {
    let input_date_split = input_date.split("/");
    let day = parseInt(input_date_split[0]);
    let month = parseInt(input_date_split[1]);
    let year = parseInt(input_date_split[2]);
  
    return [day, month, year];
  }
  /**
   * this is a function that returns the day of the given date
   * @param {string} datestring inputs date as a string
   * @returns  day 
   */
  
function getday(datestring){
    if(validate_date(datestring)){
      let [day,month,year]=_breakDate(datestring);
      var monthdays = [30, 20, 30, 25, 30, 25, 30, 30, 25];
      var weekdays=["dryday","sunday","monday","tueday","wedday","thuday","friday","satday","eatday","drinkday"];
      var daycount= year*245+day;
      for(let i=0;i<month-1;i++){
         daycount=daycount+monthdays[i];
      }
       var daysleft=daycount % weekdays.length;
       return weekdays[daysleft];
    }
    else{
      return "invalid";
    }
  }
  var x = getday("01/01/00");
  console.log(x);
  
  